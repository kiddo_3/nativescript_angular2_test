"use strict";
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var user_1 = require("../../models/user/user");
var applicationSettings = require("application-settings");
var CreateComponent = (function () {
    function CreateComponent(location) {
        this.location = location;
        //this.personList = JSON.parse(ApplicationSettings.getString("people","[]"));
        this.user = new user_1.User("", "");
    }
    CreateComponent.prototype.save = function () {
        console.log("user: " + JSON.stringify(this.user));
        if (this.user.firstname != "" && this.user.lastname != "") {
            console.log("Almacenar!");
            var people = JSON.parse(applicationSettings.getString("people", "[]"));
            people.push(this.user);
            applicationSettings.setString("people", JSON.stringify(people));
            this.location.back();
        }
    };
    CreateComponent = __decorate([
        core_1.Component({
            selector: "create",
            templateUrl: "./components/create/create.html",
        }), 
        __metadata('design:paramtypes', [common_1.Location])
    ], CreateComponent);
    return CreateComponent;
}());
exports.CreateComponent = CreateComponent;
//# sourceMappingURL=create.component.js.map