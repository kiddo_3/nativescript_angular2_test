// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic, NativeScriptModule } from "nativescript-angular/platform";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import {ListComponent} from "./components/list/list.component";
import {CreateComponent} from "./components/create/create.component";

const routes = [
    { path: "list", component: ListComponent, name: "List" },
    { path: "create", component: CreateComponent, name: "Create" },
    { path: "", redirectTo: 'list', pathMatch: "full"} //default
];

@NgModule({
    declarations: [AppComponent, ListComponent, CreateComponent],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,        
        NativeScriptFormsModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes)
        ],
})
class AppComponentModule {}

platformNativeScriptDynamic().bootstrapModule(AppComponentModule);