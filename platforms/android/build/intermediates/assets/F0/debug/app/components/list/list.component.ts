import {Component} from "@angular/core";
import {Location} from "@angular/common";
import {Router} from "@angular/Router";
import * as ApplicationSettings from "application-settings";

@Component({
    selector: "list",
    templateUrl: "./components/list/list.xml",
})
export class ListComponent {
    private router: Router;
    public personList: Array<Object>;

    constructor(router: Router, location:Location){
        this.router = router;
        this.personList = JSON.parse(ApplicationSettings.getString("people","[]"));
        location.subscribe((path)=>{ //backwards in navigator
            this.personList = JSON.parse(ApplicationSettings.getString("people","[]"));
        });
    }

    create(){
        this.router.navigate(["Create"]);
    }
}
