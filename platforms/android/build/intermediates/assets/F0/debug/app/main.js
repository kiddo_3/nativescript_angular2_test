"use strict";
// this import should be first in order to load some required settings (like globals and reflect-metadata)
var platform_1 = require("nativescript-angular/platform");
var core_1 = require("@angular/core");
var app_component_1 = require("./app.component");
var router_1 = require("nativescript-angular/router");
var list_component_1 = require("./components/list/list.component");
var create_component_1 = require("./components/create/create.component");
var routes = [
    { path: "", redirectTo: '/list', pathMatch: "full" },
    { path: "/list", component: list_component_1.ListComponent, name: "List" },
    { path: "/create", component: create_component_1.CreateComponent, name: "Create" }
];
var AppComponentModule = (function () {
    function AppComponentModule() {
    }
    AppComponentModule = __decorate([
        core_1.NgModule({
            declarations: [app_component_1.AppComponent],
            bootstrap: [app_component_1.AppComponent],
            imports: [
                platform_1.NativeScriptModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forRoot(routes)
            ],
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponentModule);
    return AppComponentModule;
}());
platform_1.platformNativeScriptDynamic().bootstrapModule(AppComponentModule);
//# sourceMappingURL=main.js.map