// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic, NativeScriptModule } from "nativescript-angular/platform";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import {ListComponent} from "./components/list/list.component";
import {CreateComponent} from "./components/create/create.component";

const routes = [
    { path: "", redirectTo: '/list', pathMatch: "full"}, //default
    { path: "/list", component: ListComponent, name: "List" },
    { path: "/create", component: CreateComponent, name: "Create" }
];

@NgModule({
    declarations: [AppComponent],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes)
        ],
})
class AppComponentModule {}

platformNativeScriptDynamic().bootstrapModule(AppComponentModule);