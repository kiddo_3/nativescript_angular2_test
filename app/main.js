"use strict";
// this import should be first in order to load some required settings (like globals and reflect-metadata)
var platform_1 = require("nativescript-angular/platform");
var core_1 = require("@angular/core");
var app_component_1 = require("./app.component");
var router_1 = require("nativescript-angular/router");
var forms_1 = require("nativescript-angular/forms");
var list_component_1 = require("./components/list/list.component");
var create_component_1 = require("./components/create/create.component");
var routes = [
    { path: "list", component: list_component_1.ListComponent, name: "List" },
    { path: "create", component: create_component_1.CreateComponent, name: "Create" },
    { path: "", redirectTo: 'list', pathMatch: "full" } //default
];
var AppComponentModule = (function () {
    function AppComponentModule() {
    }
    AppComponentModule = __decorate([
        core_1.NgModule({
            declarations: [app_component_1.AppComponent, list_component_1.ListComponent, create_component_1.CreateComponent],
            bootstrap: [app_component_1.AppComponent],
            imports: [
                platform_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule,
                router_1.NativeScriptRouterModule,
                router_1.NativeScriptRouterModule.forRoot(routes)
            ],
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponentModule);
    return AppComponentModule;
}());
platform_1.platformNativeScriptDynamic().bootstrapModule(AppComponentModule);
//# sourceMappingURL=main.js.map