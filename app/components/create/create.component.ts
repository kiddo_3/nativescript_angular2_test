import {Component} from "@angular/core";
import {Location} from "@angular/common";
import {User} from "../../models/user/user";
import * as applicationSettings from "application-settings";

@Component({
    selector: "create",
    templateUrl: "./components/create/create.html",
})



export class CreateComponent {
    private location: Location;
    private personList: Array<Object>;
    public user: User;

    constructor(location:Location){
        this.location = location;
        //this.personList = JSON.parse(ApplicationSettings.getString("people","[]"));
        this.user = new User("","");       
    }

    save(){
        console.log("user: " + JSON.stringify(this.user));
        if(this.user.firstname!= "" && this.user.lastname!= ""){
            console.log("Almacenar!");           
            var people: Array<Object> = JSON.parse(applicationSettings.getString("people", "[]"));
            people.push(this.user);
            applicationSettings.setString("people", JSON.stringify(people));
            this.location.back();
        }
    }
}
